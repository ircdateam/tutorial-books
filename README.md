### What is this repository for? ###

* Quickly learning how to use Git and Bitbucket
* Playing with Version Control

### How do I get set up? ###

* If you are using SourceTree, click the menu item on the left for Clone or the button on the top right that looks like this: ![Download.png](https://bitbucket.org/repo/dLjEBx/images/292193588-Download.png)   
* Click Clone in SourceTree. It will start SourceTree; follow the prompts there. 

* If you are not using SourceTree, set up your repo to point to https://bitbucket.org/ircdateam/tutorial-books as its origin.